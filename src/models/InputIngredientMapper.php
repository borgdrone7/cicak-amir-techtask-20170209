<?php
namespace Recipes\models;
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 13:28
 * This mapper expects array as an input, the one got from direct json_decode output
 * input data is assumed to be correct
 */
class InputIngredientMapper
{
    /*
     * @return InputIngredient
     */
    public function getMappedIngredient($ingredientData)
    {
        return new InputIngredient($ingredientData->title, $ingredientData->{'best-before'}, $ingredientData->{'use-by'});
    }

    /*
     * @return InputIngredient []
     */
    public function getMappedIngredients($ingredientsData)
    {
        $ingredients = [];
        foreach ($ingredientsData as $ingredientData) {
            $ingredients[] = $this->getMappedIngredient($ingredientData);
        }
        return $ingredients;
    }
}