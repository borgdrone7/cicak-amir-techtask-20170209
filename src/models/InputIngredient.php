<?php
namespace Recipes\models;

use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 13:28
 */
class InputIngredient
{
    protected $title;
    protected $bestBefore;
    protected $useBy;

    public function __construct($title, $bestBefore, $useBy)
    {
        $this->title = $title;
        $this->bestBefore = new Carbon($bestBefore);
        $this->useBy = new Carbon($useBy);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function usable(Carbon $date)
    {
        $l = $date->lt($this->useBy);
        return $date->lt($this->useBy);
    }

    public function fresh(Carbon $date)
    {
        return $date->lt($this->bestBefore);
    }
}