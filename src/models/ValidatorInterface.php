<?php
namespace Recipes\models;
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 13:28
 */
interface ValidatorInterface
{
    public function validate($data);
}