<?php
namespace Recipes\models;
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 13:28
 */
class InputIngredientValidator implements ValidatorInterface
{
    protected $validator;
    protected $schema;

    public function __construct($validator, $schema)
    {
        $this->validator = $validator;
        $this->schema = $schema;
    }

    public function validate($data)
    {
        $errs = [];
        $res = $this->validator->validate($data, $this->schema);
        $errs = array_map(function ($x) {
            return $x->message;
        }, $res->errors);
        return (object)["valid" => $res->valid, "errors" => $errs];
    }
}