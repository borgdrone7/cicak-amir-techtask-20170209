<?php
namespace Recipes\models;
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 13:28
 * This mapper expects array as an input, the one got from direct json_decode output
 * input data is assumed to be correct
 */
class RecipeArrayMapper implements RecipeMapperInterface
{
    /*
     * @return Recipe
     */
    public function getMappedRecipe($recipeData)
    {
        $recipe = new Recipe();
        $recipe->setTitle($recipeData->title);
        foreach ($recipeData->ingredients as $ingredient) {
            $recipe->addIngredient($ingredient);
        }
        return $recipe;
    }

    /*
     * @return Recipe []
     */
    public function getMappedRecipes($recipesArrayData)
    {
        $recipes = [];
        foreach ($recipesArrayData as $recipeData) {
            $recipes[] = $this->getMappedRecipe($recipeData);
        }
        return $recipes;
    }
}