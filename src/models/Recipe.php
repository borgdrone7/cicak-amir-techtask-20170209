<?php
namespace Recipes\models;
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 13:28
 */
class Recipe
{
    protected $title;
    protected $ingredients;
    public $score;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function addIngredient($title)
    {
        $this->ingredients[] = new RecipeIngredient($title);
    }

    /*
     * @return RecipeIngredient []
    */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    public function toArray()
    {
        return ["title" => $this->getTitle(), "score" => round($this->score, 2)];
    }
}