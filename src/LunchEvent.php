<?php
namespace Recipes;

use Symfony\Component\EventDispatcher\Event;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-25
 * Time: 16:36
 */
class LunchEvent extends Event
{
    const NAME = "lunch.found";
    protected $lunches;

    public function __construct($lunches)
    {
        $this->lunches = $lunches;
    }

    public function getLunches()
    {
        $lunches = array_map(function ($l) {
            return $l["title"];
        }, $this->lunches);
        return implode(", ", $lunches);
    }
}