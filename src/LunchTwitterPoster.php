<?php
namespace Recipes;

use Abraham\TwitterOAuth\TwitterOAuth;
use Monolog\Logger;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-25
 * Time: 16:34
 */
class LunchTwitterPoster
{
    protected $twitter;
    protected $log;
    protected $messagePrefix;

    public function __construct($twitter_creds, Logger $log, $prefix)
    {
        $this->log = $log;
        $this->twitter = new TwitterOAuth(
            $twitter_creds["CONSUMER_KEY"],
            $twitter_creds["CONSUMER_SECRET"],
            $twitter_creds["access_token"],
            $twitter_creds["access_token_secret"]
        );
        $this->messagePrefix=empty($prefix) ? "Today we could have these for lunch: ":$prefix;
    }

    public function post(LunchEvent $event)
    {
        $statuses = $this->twitter->post("statuses/update", ["status" => $this->messagePrefix . $event->getLunches() . "."]);
        if (!empty($statuses->errors) && count($statuses->errors)) {
            foreach ($statuses->errors as $error) {
                $this->log->addError("Couldn't post to twitter: " . $statuses->errors[0]->message);
            }
            return;
        }
        if ($this->twitter->getLastHttpCode() != "200") {
            $this->log->addError("Couldn't post to twitter, response code: " . $this->twitter->getLastHttpCode());
        }
    }
}