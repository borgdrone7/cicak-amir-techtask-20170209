<?php
namespace Recipes\repos;
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 14:07
 */
class RecipesJsonRepo implements RecipesRepoInterface
{
    protected $filePath;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    public function loadRecipes()
    {
        return json_decode(file_get_contents($this->filePath))->recipes;
    }
}