<?php
namespace Recipes\repos;
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 14:07
 */
interface RecipesRepoInterface
{
    public function loadRecipes();
}