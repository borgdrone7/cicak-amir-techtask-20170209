<?php
namespace Recipes\controllers;

use Monolog\Logger;
use Recipes\LunchEvent;
use Recipes\matchers\RecipeMatcherInterface;
use Recipes\models\InputIngredientMapper;
use Recipes\models\InputIngredientValidator;
use Recipes\models\Recipe;
use Recipes\models\ValidatorInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 21:34
 */
class RecipesController
{
    protected $validator;
    protected $dispatcher;
    protected $matcher;
    protected $logger;

    public function __construct(
        RecipeMatcherInterface $matcher,
        ValidatorInterface $validator,
        EventDispatcher $dispatcher,
        Logger $logger)
    {
        $this->validator = $validator;
        $this->dispatcher = $dispatcher;
        $this->matcher = $matcher;
        $this->logger = $logger;
    }

    public function lunch(Request $request)
    {
        $postedData = json_decode($request->getContent());
        if (empty($postedData)) {
            $res = (object)["status" => "Bad request, json incorrect!"];
            return new JsonResponse($res, 400);
        }
        $res = $this->validator->validate($postedData);
        if (!$res->valid) {
            $res->status = "Bad request, json format not matching schema!";
            return new JsonResponse($res, 400);
        }
        try {
            $mapper = new InputIngredientMapper();
            $ingredients = $mapper->getMappedIngredients($postedData->ingredients);
            $matchedRecipes = $this->matcher->match($ingredients);
            $outputRecipes = array_map(function (Recipe $r) {
                return $r->toArray();
            }, $matchedRecipes);
            if (count($outputRecipes) > 0 && (!empty($postedData->twitter) && $postedData->twitter)) {
                $this->dispatcher->dispatch(LunchEvent::NAME, new LunchEvent($outputRecipes));
            }
            return new JsonResponse($outputRecipes);
        } catch (\Exception $e) {
            $res = (object)["status" => "Internal server error: " . $e->getMessage()];
            $this->logger->addError("Error while matching lunch: ".$e->getMessage());
            return new JsonResponse($res, 500);
        }
    }
}