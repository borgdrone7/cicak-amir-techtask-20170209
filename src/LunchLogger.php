<?php
namespace Recipes;

use Monolog\Logger;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-25
 * Time: 16:34
 */
class LunchLogger
{
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function log(LunchEvent $event)
    {
        $this->logger->addInfo("Today we could have these for lunch: " . $event->getLunches() . ".");
    }
}