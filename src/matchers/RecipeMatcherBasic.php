<?php
namespace Recipes\matchers;

use Carbon\Carbon;
use Recipes\models\InputIngredient;
use Recipes\models\Recipe;
use Recipes\models\RecipeIngredient;
use Recipes\models\RecipeMapperInterface;
use Recipes\repos\RecipesRepoInterface;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-25
 * Time: 13:27
 */
class RecipeMatcherBasic implements RecipeMatcherInterface
{
    protected $recipesMapper;
    protected $recipesRepo;
    protected $today;

    public function __construct(RecipesRepoInterface $repo, RecipeMapperInterface $mapper, Carbon $today)
    {
        $this->recipesMapper = $mapper;
        $this->recipesRepo = $repo;
        $this->today = $today;
    }

    public function match($ingredients)
    {
        $recipes = $this->recipesMapper->getMappedRecipes($this->recipesRepo->loadRecipes());
        $goodRecipes = [];
        /** @var Recipe $recipe */
        foreach ($recipes as $recipe) {
            $recipe->score = $this->calculateScore($recipe, $ingredients);
            if ($recipe->score > 0) {
                $goodRecipes[] = $recipe;
            }
        }
        $cmp = function ($a, $b) {
            if ($a->score == $b->score) {
                return 0;
            }
            return ($a->score > $b->score) ? -1 : 1;
        };
        usort($goodRecipes, $cmp);
        return $goodRecipes;
    }

    /**
     * @param Recipe $recipe
     * @param InputIngredient [] $ingredients
     */
    protected function calculateScore(Recipe $recipe, $ingredients)
    {
        $score = 0;
        $recipeIngredients = $recipe->getIngredients();
        /** @var RecipeIngredient $recipeIngredient */
        foreach ($recipeIngredients as $recipeIngredient) {
            $foundIngredient = $this->findIngredient($recipeIngredient, $ingredients);
            if (empty($foundIngredient)) {
                return -1;
            }
            $score += 0.5 + 0.5 * $foundIngredient->fresh($this->today);
        }
        //normalize the score
        $score /= count($recipeIngredients);
        return $score;
    }

    /**
     * @param RecipeIngredient $needle
     * @param InputIngredient [] $haystack
     */
    protected function findIngredient(RecipeIngredient $needle, $haystack)
    {
        foreach ($haystack as $inputIngredient) {
            if ($inputIngredient->getTitle() == $needle->getTitle() && $inputIngredient->usable($this->today)) {
                return $inputIngredient;
            }
        }
        return null;
    }
}