<?php
namespace Recipes\matchers;
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-25
 * Time: 13:25
 */
interface RecipeMatcherInterface
{
    public function match($ingredients);
}