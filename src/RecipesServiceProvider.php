<?php
namespace Recipes;

use Carbon\Carbon;
use JDesrosiers\Silex\Provider\Jsv4Validator;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\WhatFailureGroupHandler;
use Monolog\Logger;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Recipes\matchers\RecipeMatcherBasic;
use Recipes\models\InputIngredientValidator;
use Recipes\models\RecipeArrayMapper;
use Recipes\repos\RecipesJsonRepo;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-25
 * Time: 20:03
 */
class RecipesServiceProvider implements ServiceProviderInterface
{

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $app)
    {
        $this->createLogger($app);
        $this->setupEvents($app);
        $app["recipes.recipes_json"] = __DIR__ . "/../var/data/recipes.json";
        $app["recipes.repo"] = new RecipesJsonRepo($app["recipes.recipes_json"]);
        $app["recipes.ingredient_validator_schema"] = file_get_contents(__DIR__ . "/../var/data/ingredients_schema.json");
        $app["recipes.ingredient_input_validator"] = new InputIngredientValidator(
            new Jsv4Validator(),
            json_decode($app["recipes.ingredient_validator_schema"])
        );
        $app["recipes.today"] = new Carbon("2017-02-24");
        $app["recipes.recipe_mapper"] = new RecipeArrayMapper();
        $app["recipes.recipe_matcher"] = new RecipeMatcherBasic($app["recipes.repo"], $app["recipes.recipe_mapper"], $app["recipes.today"]);
    }

    protected function createLogger($app)
    {
        $app["recipes.logger_path"] = __DIR__ . '/../var/logs/recipes.log';
        $log_group = new WhatFailureGroupHandler([
                new RotatingFileHandler($app["recipes.logger_path"], 365, Logger::INFO)
            ]
        );
        $app["recipes.logger"] = new Logger("Recipes logger", [$log_group]);
    }

    protected function setupEvents($app)
    {
        $app["twitter.message_prefix"]="We cound eat these today for example: ";
        $app["twitter.key"]="hw3E91QucOuuwNm0ZhriYZdCW";
        $app["twitter.secret"]="ztsc6hA2CICNFuX1SK7BEvFBeguVR3xcAtUBnsTwNv0cORZq5J";
        $app["twitter.token"]="835317208020619264-yBkjju3E9nOkdGTtTfFWN9e5pixxaTW";
        $app["twitter.token_secret"]="LqCOWdH0f5tWgf5BS9vCzWi1HwAKipGeMOQygnB1TeYoo";

        $lunchEventDispatcher = new EventDispatcher();
        $lunchEventDispatcher->addListener(LunchEvent::NAME, [new LunchLogger($app["recipes.logger"]), "log"]);
        $lunchEventDispatcher->addListener(
            LunchEvent::NAME,
            [
                new LunchTwitterPoster(
                    [
                        "CONSUMER_KEY"        => $app["twitter.key"],
                        "CONSUMER_SECRET"     => $app["twitter.secret"],
                        "access_token"        => $app["twitter.token"],
                        "access_token_secret" => $app["twitter.token_secret"]
                    ],
                    $app["recipes.logger"],
                    $app["twitter.message_prefix"]
                ),
                "post"
            ]
        );
        $app["lunch.event_dispatcher"] = $lunchEventDispatcher;
    }
}