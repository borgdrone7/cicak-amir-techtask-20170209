<?php

use Recipes\controllers\RecipesController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//Request::setTrustedProxies(['172.18.0.3']);
$app['recipes.controller'] = function () use ($app) {
    return new RecipesController(
        $app["recipes.recipe_matcher"],
        $app["recipes.ingredient_input_validator"],
        $app["lunch.event_dispatcher"],
        $app["recipes.logger"]
    );
};
$app->post('/lunch', "recipes.controller:lunch");

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', []);
})->bind('homepage');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = [
        'errors/' . $code . '.html.twig',
        'errors/' . substr($code, 0, 2) . 'x.html.twig',
        'errors/' . substr($code, 0, 1) . 'xx.html.twig',
        'errors/default.html.twig',
    ];

    return new Response($app['twig']->resolveTemplate($templates)->render(['code' => $code]), $code);
});
