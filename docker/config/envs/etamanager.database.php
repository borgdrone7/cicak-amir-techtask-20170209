<?php

return [

	'connections' => [

		'etalincalc' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'etalincalc',
			'username'  => 'etalincalc',
			'password'  => 'Eta1024!',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
            'log'       => true,
			'prefix'    => ''
		],
		'etameasure' => [
			'driver'    => 'mysql',
			'host'      => 'etamanager_db_1',
			'database'  => 'etamanager',
			'username'  => 'xpreso',
			'password'  => 'Xx1024!',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
            'log'       => true,
			'prefix'    => ''
		],
		'xpreso' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'db_xpresodeliver',
			'username'  => 'xpreso',
			'password'  => 'Eta1024!',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
            'log'       => true,
			'prefix'    => ''
		]

	]

];
