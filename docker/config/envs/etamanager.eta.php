<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 3/13/2015
 * Time: 1:22 PM
 */
use Monolog\Logger;

return [
    'host'          => 'mq-aws-us-east-1.iron.io',
    'token'   		=> 'OtawrlqkahM8BHN6srVoXi7Lp78',
    'project' 		=> '54cf5e6e6ae8d30009000024',
    'queue'   		=> 'manifests',
    'verify_peer' 	=> false,
    'errqueue' 		=> 'manifests_error', //failed messages queue
    'security_token' => '!Xx2048xX!',
    'responseEndpoint' => 'http://platform.dev/er/',
    'responseEndpointProxy' => false,
    'calculatorDb'=> 'etalincalc',
    'measureDb'=> 'etameasure',
    'slackLog'=>'https://hooks.slack.com/services/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',
    'slackLogLevel'=>Logger::CRITICAL, //change to Logger::CRITICAL to disable slack
    'measureGuiTimeZone' => 'Europe/Dublin',
    'paperLog'=>"logs2.papertrailapp.com",
    'paperLogPort'=>"24511",
    'paperLogLevel'=>Logger::INFO,
    'logentries'        => "9a18ea97-1079-4019-8ff3-96334cce94d5",
    'logentriesLevel'   => Logger::INFO,
    'bambooVersion' => 'Local version 1.0',
    'esri_client_id' => 'EPExhx2NdcHxKYm7',
    'esri_client_secret' => 'a43f9ed9ad8d4484b4f0fc8297ff5b07',
    'agent' => "BorgDrone",
    'googleMapsKey' => 'AIzaSyBlGq1wmY6XoZlr5geBq1nLN3KXNFx9QVM'
];