#!/bin/sh
docker network create ac
docker-compose -f recipes.yml -p recipes up -d --build
docker-compose -f front.yml -p front up -d --build
docker-compose -f acbase.yml -p acbase up -d workspace
