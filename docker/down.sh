#!/bin/sh
docker-compose -f front.yml -p front down --remove-orphans
docker-compose -f acbase.yml -p acbase down
docker-compose -f recipes.yml -p recipes down --remove-orphans

docker network rm ac
