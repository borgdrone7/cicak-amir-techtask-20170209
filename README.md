Hi,
I am Amir Cicak and this is engineering tech task for ManageFlitter.
If using Docker. Simply run ./up.sh and all containers will start.
You can access the app either using **recipes.dev** (if you add it to point to localhost in hosts file), or at **http://localhost:8071/**.
Actually on my machine nginx accepts recipes.dev so I can have more project accessed on port 80 using another nginx reverse proxy.
As you use Vagrant I modified nginx to accept everything and be default server.
I manage also to set it up to work with Vagrant on my machine. As I use Windows and my vritualization choice is Hyper-V, I hope the same will work on different setups. 
Anyway, Vagrantfile is included, BUT before you start vagrant up you must install docker compose plugin:
Steps for vagrant:

1a. vagrant plugin install vagrant-docker-compose
2a. if you are not using Hyper-V, remove this line from Vagrantfile:  config.vm.provider "hyperv"
(Update, I commented that line, enable it only if using hyper-v).
3a. vagrant up
4a. in browser type: 192.168.0.200:8071
5a. access to lunch post endpoint at 192.168.0.200:8071/lunch
6a. Eaxmple curl request (split in multiple lines, should be one) is at bottom of this document
If for some reason that IP (192.168.0.200) is not assigned:
1b. vagrant ssh
2b. ifconfig -a
3b. look for eth0 inet addr:***** (for example 192.168.0.140)
4b. do steps starting from 4a

If you get error: /bin/sh^M invalid interpreter during vagrant up, it is probably because in the process git or something else changed line endings from Unix to Windows style.
You can convert them with dos2unix:
1. sudo apt-get install dos2unix
2. dos2unix up.sh
3. you can manually start up (make sure you are in /vagrant/docker foler) ./up.sh, or you can vagrant up --provision after that

Please note that current docker configuration is not optimal if we are just to use Vagrant.
It is because it includes 2 nginx instances, one is reverse proxy and one real nginx.
As I only use docker (without Vagrant) it makes sense since I can run as much projects as I want on the same machine with one VM.
Also all projects can run on port 80 and web site selection is done using nginx server_name.
Now I disabled that (I use recipes.dev) as I don't want you to deal with hosts file.
So now under vagrant one nginx container extra will be run and app will be acceessed using IP.
Also as I set it to listen to anything it is not optimal for my local Docker as I want it only to listen to recipes.dev.
Anyway, I set it in git like that but locally I use optimal Docker configuration.

I saved also a video file explaining briefly what was done, here: https://youtu.be/LuqniEgcpNE
The video is unlisted so no one should be able to find it without direct link.

Curl call:

curl -H "Content-Type: application/json" -X POST -d '{
   "twitter": false,
   "ingredients": [
     {
       "title": "Lettuce",
       "best-before": "2017-02-28",
       "use-by": "2017-02-27"
     },
     {
       "title": "Tomato",
       "best-before": "2017-02-25",
       "use-by": "2017-02-27"
     },
     {
       "title": "Cucumber",
       "best-before": "2017-02-25",
       "use-by": "2017-02-27"
     },
     {
       "title": "Beetroot",
       "best-before": "2017-02-25",
       "use-by": "2017-02-27"
     },
     {
       "title": "Salad Dressing",
       "best-before": "2017-02-06",
       "use-by": "2017-03-07"
     },
     {
       "title": "Bacon",
       "best-before": "2017-02-25",
       "use-by": "2017-02-27"
     },
     {
       "title": "Eggs",
       "best-before": "2017-02-25",
       "use-by": "2017-02-27"
     },
     {
       "title": "Mushrooms",
       "best-before": "2017-02-25",
       "use-by": "2017-02-27"
     },
     {
       "title": "Sausage",
       "best-before": "2017-02-25",
       "use-by": "2017-02-27"
     },
     {
       "title": "Bread",
       "best-before": "2017-02-25",
       "use-by": "2017-02-27"
     },
     {
       "title": "Baked Beans",
       "best-before": "2017-02-06",
       "use-by": "2017-02-27"
     }
   ]
 }' http://192.168.0.200:8071/lunch