<?php
use Recipes\models\Recipe;
use Recipes\models\RecipeArrayMapper;
use Recipes\repos\RecipesJsonRepo;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 13:23
 */
class RecipesTest extends PHPUnit_Framework_TestCase
{
    public function testPhpUnit()
    {
        $this->assertEquals(true, true);
    }

    protected function getJsonRecipeData()
    {
        $repo=new RecipesJsonRepo(__DIR__ . "/../var/data/recipes.json");
        return $repo->loadRecipes();
    }

    public function testRepo()
    {
        $data=$this->getJsonRecipeData();
        $this->assertEquals(count($data), 4);
        $this->assertEquals($data[0]->title, "Ham and Cheese Toastie");
        $this->assertEquals($data[1]->ingredients[3], "Mushrooms");
    }

    public function testMapper()
    {
        $mapper=new RecipeArrayMapper();
        $recipe=$mapper->getMappedRecipe($this->getJsonRecipeData()[0]);
        $this->assertEquals($recipe->getTitle(), "Ham and Cheese Toastie");
        $this->assertEquals($recipe->getIngredients()[2]->getTitle(), "Bread");
    }
}