<?php

use Silex\WebTestCase;

class ControllersTest extends WebTestCase
{
    public function testGetHomepage()
    {
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('Welcome', $crawler->filter('body')->text());
    }

    public function testLunch()
    {
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler=$client->request(
            'POST', '/lunch', [], [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X-Requested-With' => 'XMLHttpRequest'
            ],
            file_get_contents(__DIR__ . "/../var/data/ingredients.json")
        );
        $data = $client->getResponse()->getContent();
        $this->assertTrue($client->getResponse()->isOk());
    }
    
    public function createApplication()
    {
        $app = require __DIR__.'/../src/app.php';
        require __DIR__.'/../config/dev.php';
        require __DIR__.'/../src/controllers.php';
        $app['session.test'] = true;

        return $this->app = $app;
    }
}
