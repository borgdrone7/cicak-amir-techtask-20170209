<?php
use Abraham\TwitterOAuth\TwitterOAuth;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-25
 * Time: 17:06
 */
class TwitterTest extends PHPUnit_Framework_TestCase
{
    protected function getConnection()
    {
        return new TwitterOAuth(
            "hw3E91QucOuuwNm0ZhriYZdCW",
            "ztsc6hA2CICNFuX1SK7BEvFBeguVR3xcAtUBnsTwNv0cORZq5J",
            "835317208020619264-yBkjju3E9nOkdGTtTfFWN9e5pixxaTW",
            "LqCOWdH0f5tWgf5BS9vCzWi1HwAKipGeMOQygnB1TeYoo"
        );
    }
    public function testTwitter()
    {
        $connection=$this->getConnection();
        $content = $connection->get("account/verify_credentials");
        $this->assertEquals("Borg Drone", $content->name);
    }

    public function testTwitterPost()
    {
        $connection=$this->getConnection();
        $statuses = $connection->post("statuses/update", ["status" => "hello world"]);
        if(!empty($statuses->errors) && count($statuses->errors)) {
            $this->assertEquals("187", $statuses->errors[0]->code);
            return;
        }
        $this->assertEquals("200", $connection->getLastHttpCode());
    }
}