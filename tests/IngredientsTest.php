<?php
use Recipes\models\InputIngredient;
use Recipes\models\InputIngredientMapper;
use Recipes\models\InputIngredientValidator;
use Silex\WebTestCase;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-24
 * Time: 13:23
 */
class IngredientsTest extends WebTestCase
{
    public function testValidator()
    {
        $validator=new InputIngredientValidator(
            $this->app["json-schema.validator"],
            json_decode(file_get_contents(__DIR__ . "/../var/data/ingredients_schema.json"))
        );
        $res=$validator->validate(json_decode(file_get_contents(__DIR__ . "/../var/data/ingredients_failure.json")));
        //json_encode($res, JSON_PRETTY_PRINT);
        $this->assertEquals(2, count($res->errors));

        $res=$validator->validate(json_decode(file_get_contents(__DIR__ . "/../var/data/ingredients.json")));
        $this->assertEquals(1, $res->valid);
    }

    public function testMapper()
    {
        $data=json_decode(file_get_contents(__DIR__ . "/../var/data/ingredients.json"));
        $mapper=new InputIngredientMapper();
        $ingredients=$mapper->getMappedIngredients($data->ingredients);
        $this->assertCount(16, $ingredients);
        $this->assertInstanceOf(InputIngredient::class, $ingredients[0]);
        $this->assertSame($ingredients[2]->getTitle(), "Bread");
    }
    public function createApplication()
    {
        $app = require __DIR__.'/../src/app.php';
        require __DIR__.'/../config/dev.php';
        require __DIR__.'/../src/controllers.php';
        $app['session.test'] = true;

        return $this->app = $app;
    }
}