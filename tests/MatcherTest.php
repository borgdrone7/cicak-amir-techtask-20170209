<?php
use Carbon\Carbon;
use Recipes\matchers\RecipeMatcherBasic;
use Recipes\models\InputIngredientMapper;
use Recipes\models\Recipe;
use Recipes\models\RecipeArrayMapper;
use Recipes\repos\RecipesJsonRepo;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-02-25
 * Time: 13:28
 */
class MatcherTest extends PHPUnit_Framework_TestCase
{
    public function testBasicMatcher()
    {
        $recipe_mapper=new RecipeArrayMapper();
        $repo=new RecipesJsonRepo(__DIR__ . "/../var/data/recipes.json");
        $data=json_decode(file_get_contents(__DIR__ . "/../var/data/ingredients_salad.json"));
        $mapper=new InputIngredientMapper();
        $ingredients=$mapper->getMappedIngredients($data->ingredients);
        $matcher=new RecipeMatcherBasic($repo, $recipe_mapper, new Carbon("2017-02-25"));
        /** @var Recipe [] $output */
        $output=$matcher->match($ingredients);
        $this->assertCount(1, $output);
        $this->assertEquals("Salad", $output[0]->getTitle());

        $data=json_decode(file_get_contents(__DIR__ . "/../var/data/ingredients.json"));
        $ingredients=$mapper->getMappedIngredients($data->ingredients);
        /** @var Recipe [] $output */
        $output=$matcher->match($ingredients);
        $this->assertCount(1, $output);
        $this->assertEquals("Hotdog", $output[0]->getTitle());

        $data=json_decode(file_get_contents(__DIR__ . "/../var/data/ingredients_salad_and_fry_up.json"));
        $ingredients=$mapper->getMappedIngredients($data->ingredients);
        /** @var Recipe [] $output */
        $output=$matcher->match($ingredients);
        $this->assertCount(2, $output);
        $this->assertEquals("Fry-up", $output[1]->getTitle());

        $repo=new RecipesJsonRepo(__DIR__ . "/../var/data/recipes_fresh_test.json");
        $matcher=new RecipeMatcherBasic($repo, $recipe_mapper, new Carbon("2017-02-24"));
        $data=json_decode(file_get_contents(__DIR__ . "/../var/data/ingredients_fresh_test.json"));
        $ingredients=$mapper->getMappedIngredients($data->ingredients);
        /** @var Recipe [] $output */
        $output=$matcher->match($ingredients);
//        var_dump($output);
        $this->assertCount(2, $output);
        $this->assertEquals("Fry-up", $output[0]->getTitle());
    }
}